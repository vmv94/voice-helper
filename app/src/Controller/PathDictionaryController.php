<?php

namespace App\Controller;

use App\Entity\PathDictionary;
use App\Service\NormalizeService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/api/path")
 */
class PathDictionaryController extends AbstractController
{
    /**
     * @Route("/", name="get_paths", methods={"GET"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function getPaths(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $type = $request->query->get('type');
        $repo = $em->getRepository(PathDictionary::class);

        if ($type)
            $paths = $repo->findByType($type);
        else
            $paths = $repo->findAll();

        return $this->json([
            'message' => 'Все пути из словаря',
            'data' => (new NormalizeService())->normalizeByGroup($paths),
        ]);
    }

    /**
     * @Route("/", name="add_path", methods={"POST"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function addPath(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->toArray();

        if(
            empty($payload = $data['payload']) ||
            empty($type = $data['type'])
        )
            return $this->json([
                'message' =>'Не введена вся требуемая информация'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);

        $newPath = new PathDictionary();

        try {
            $newPath->setType($type);
        } catch (\InvalidArgumentException $e) {
            return $this->json([
                'message' =>'Не существует такого типа пути'
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $newPath->setPayload($payload);
        $newPath->setParameters(
            is_array($parameters = $data['parameters'] ?? array())
            ? $parameters : array()
        );
        $newPath->setDescription($data['description'] ?? null);
        $newPath->setNeedAuth(intval($data['need_auth'] ?? 0) === 1);

        $em->persist($newPath);
        $em->flush();

        return $this->json([
            'message' => 'Новый путь добавлен в словарь',
            'data' => (new NormalizeService())->normalizeByGroup($newPath),
        ]);
    }

    /**
     * @Route("/update", name="update_path", methods={"POST"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function updatePath(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->toArray();

        if(is_null($path = $em->getRepository(PathDictionary::class)->find($data['id'] ?? '')))
            return $this->json([
                'message' =>'Нет такого пути'
            ], Response::HTTP_NOT_FOUND);

        $path->setType($data['type'] ?? $path->getType());
        $path->setPayload($data['payload'] ?? $path->getPayload());
        $path->setParameters(
            is_array($parameters = $data['parameters'] ?? '')
            ? $parameters : $path->getParameters()
        );
        $path->setDescription($data['description'] ?? $path->getDescription());
        $path->setNeedAuth(intval($data['need_auth'] ?? 0) === 1);

        $em->flush();

        return $this->json([
            'message' => 'Путь был успешно обновлен',
            'data' => (new NormalizeService())->normalizeByGroup($path),
        ]);
    }

    /**
     * @Route("/parameters", name="update_path_parameters", methods={"POST"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function updatePathParameters(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->toArray();

        if(is_null($path = $em->getRepository(PathDictionary::class)->find($data['id'] ?? '')))
            return $this->json([
                'message' =>'Нет такого пути'
            ], Response::HTTP_NOT_FOUND);

        if(!is_null($parameters = $data['parameters'] ?? null)) {
            $type = $data['type'] ?? 'add';
            $currentParams = $path->getParameters();
            if(is_array($parameters)) {
                if($type == 'add') {
                    foreach($parameters as $parameter) {
                        $currentParams[] = $parameter;
                    }
                }
                elseif($type == 'remove') {
                    foreach($parameters as $parameter) {
                        for($i = 0; $i < count($currentParams); $i++) {
                            if(
                                $currentParams[$i]['name'] == $parameter['name'] &&
                                $currentParams[$i]['value'] == $parameter['value']
                            ) {
                                unset($currentParams[$i]);
                            }
                        }
                    }
                }
            }
            $path->setParameters($currentParams);
            $em->flush();
        }

        return $this->json([
            'message' => 'Параметры пути были успешно обновлены',
            'data' => (new NormalizeService())->normalizeByGroup($path),
        ]);
    }

    /**
     * @Route("/", name="del_path", methods={"DELETE"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function removePath(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->toArray();

        if(
            !isset($data['id']) ||
            is_null($path = $em->getRepository(PathDictionary::class)->find($data['id'] ?? ''))
        )
            return $this->json([
                'message' =>'Не существует такого пути'
            ], Response::HTTP_NOT_FOUND);

        $em->remove($path);
        $em->flush();

        return $this->json([
            'message' => 'Путь был удален',
        ]);
    }
}

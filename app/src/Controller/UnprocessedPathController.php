<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\UnprocessedPath;
use App\Service\NormalizeService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/unprocessed-paths")
 */
class UnprocessedPathController extends AbstractController
{
    /**
     * @Route("/", name="get unregistered requests", methods={"GET"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function getUnprocessedPath(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $project_id = $request->query->get('project') ?? '';

        $project = $em->getRepository(Project::class)->find($project_id);

        if(!in_array("ROLE_ADMIN", $this->getUser()->getRoles())
            && (is_null($project->getManager())
                || $project->getManager()->getId() != $this->getUser()->getId()))
            return $this->json([
                'message' => 'Вы не назначены на данный проект',
            ]);

        $unprocessed_path = $em
            ->getRepository(UnprocessedPath::class)
            ->findByProject($project_id);

        if (empty($unprocessed_path))
            return $this->json(['message' => 'Необработанные запросы для данного проекта отсутствуют'], Response::HTTP_NOT_FOUND);

        return $this->json([
            'message' => 'Все неотработанные запросы проекта',
            'data' => (new NormalizeService())->normalizeByGroup($unprocessed_path),
        ]);
    }

    /**
     * @Route("/", name="delete", methods={"DELETE"})
     * @IsGranted("ROLE_MANAGER")
     */
    public function deleteUnprocessedPath(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $up_id = $request->query->get('id') ?? '';

        $unprocessed_path = $em->getRepository(UnprocessedPath::class)->find($up_id);

        if (empty($unprocessed_path))
            return $this->json(['message' => 'Не существует такого проекта'], Response::HTTP_NOT_FOUND);

        $project = $em->getRepository(Project::class)->find($unprocessed_path->getId());

        if(!in_array("ROLE_ADMIN", $this->getUser()->getRoles())
            && (is_null($project->getManager())
                || $project->getManager()->getId() != $this->getUser()->getId()))
            return $this->json([
                'message' => 'Вы не назначены на данный проект',
            ]);

        $em->remove($unprocessed_path);
        $em->flush();

        return $this->json([
            'message' => 'Требуемая информаця успешно удалена',
        ]);
    }
}

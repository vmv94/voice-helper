<?php

namespace App\Entity;

use App\Repository\UnprocessedPathRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UnprocessedPathRepository::class)
 */
class UnprocessedPath
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"main"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"main"})
     */
    private $text;

    /**
     * @ORM\Column(type="bigint")
     * @Groups({"main"})
     */
    private $count = 1;

    /**
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="unprocessedPaths")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=false)
     * @Groups({"main"})
     */
    private $project;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getCount(): ?string
    {
        return $this->count;
    }

    public function setCount(string $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }
}

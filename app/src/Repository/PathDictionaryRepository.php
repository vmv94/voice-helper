<?php

namespace App\Repository;

use App\Entity\PathDictionary;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PathDictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method PathDictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method PathDictionary[]    findAll()
 * @method PathDictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PathDictionaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PathDictionary::class);
    }

    /*
    public function findOneBySomeField($value): ?PathDictionary
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

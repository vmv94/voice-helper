<?php

namespace App\Repository;

use App\Entity\FindDictionary;
use App\Entity\PathDictionary;
use App\Entity\Project;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FindDictionary|null find($id, $lockMode = null, $lockVersion = null)
 * @method FindDictionary|null findOneBy(array $criteria, array $orderBy = null)
 * @method FindDictionary[]    findAll()
 * @method FindDictionary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FindDictionaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FindDictionary::class);
    }

    /**
     * @return PathDictionary[] Returns an array of PathDictionary objects
     */
    public function findByProjectWithTypes(Project $p, string $type)
    {
        return $this->createQueryBuilder('fd')
            ->where('fd.project = :project')
            ->setParameter('project', $p->getId())
            ->innerJoin(PathDictionary::class, 'j', 'with', 'fd.path = j')
            ->andWhere('j.type = :type')
            ->setParameter('type', $type)
            ->getQuery()
            ->getResult()
            ;
    }

    /**
     * @return PathDictionary[] Returns an array of PathDictionary objects
     */
    public function findAllProjectsWithTypes(string $type)
    {
        return $this->createQueryBuilder('fd')
            ->innerJoin(PathDictionary::class, 'j', 'with', 'fd.path = j')
            ->where('j.type = :type')
            ->setParameter('type', $type)
            ->getQuery()
            ->getResult()
            ;
    }

    /*
    public function findOneBySomeField($value): ?FindDictionary
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
